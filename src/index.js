import axios from 'axios';

const RequestApi = {
  install(Vue) {
    Vue.prototype.$requestApi = {
      get(endpoint, callback, params) {
        Vue.prototype._$request(endpoint, callback, params);
      },
      post(endpoint, callback, params) {
        Vue.prototype._$request(endpoint, callback, params, "post");
      },
    }
    /**
     * required:
     *  endpoint string
     *  callback function callback function can be exexuted finally
     */
    Vue.prototype._$request = (endpoint, callback, params, method = "get") => {
      // Validation required properties
      if (!endpoint || !callback) {
        throw Error("Can not find required property.");
      }
      // check endpoint is string
      if (typeof endpoint != 'string') {
        throw Error("endpoint should be string");
      }
      // check callback is method
      if (typeof callback != 'function') {
        throw Error("callback should be function");
      }
      let return_value = {
        "result": false,
        "data": null
      };
      axios[method](endpoint, params)
        .then(response => {
          return_value.result = true;
          return_value.data = response.data;
        })
        .catch(error => {
          return_value.result = false;
          return_value.data = error;
        })
        .finally(() => callback(return_value));
    }
  }
}
export default RequestApi;