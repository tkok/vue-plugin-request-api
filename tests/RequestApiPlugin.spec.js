// test
import { createLocalVue } from '@vue/test-utils'
import Promise from 'es6-promise';
import axios from 'axios';
import RequestApi from '../src/index';

jest.mock('axios');

const localVue = createLocalVue();
localVue.use(RequestApi);

describe('Test for RequestApiPlugin', () => {
  test('get is function', () => {
    expect(typeof localVue.prototype.$requestApi.get).toBe("function");
  });

  test('get should be passed two argument', () => {
    function get_api_error() {
      localVue.prototype.$requestApi.get(1);
    }
    expect(get_api_error).toThrow("Can not find");
    function get_api_ok() {
      localVue.prototype.$requestApi.get(1, 2);
    }
    expect(get_api_ok).not.toThrow("Can not find");
  });

  test('get first argument should be string', () => {
    expect(() => { localVue.prototype.$requestApi.get(1, 1) })
      .toThrow("endpoint should be string");
    expect(() => { localVue.prototype.$requestApi.get("1", 1) })
      .not.toThrow("endpoint should be string");
  });

  test('get second argument should be function', () => {
    expect(() => { localVue.prototype.$requestApi.get("1", 1) })
      .toThrow("callback should be function");
    expect(() => { localVue.prototype.$requestApi.get("1", function () { }) })
      .not.toThrow("callback should be function");
  });
  test('get execute expected callback and catch result as success ' +
    'if response is success', done => {
      const localVue = createLocalVue();
      localVue.use(RequestApi);

      let stubData = { test_a: 1, test_b: "hello" };
      const resp = { status: 200, data: stubData };
      let resolved = Promise.resolve(resp);
      axios.get.mockImplementation(
        () => resolved
      );
      function callback(data) {
        expect(data.result).toBe(true);
        expect(data.data).toBe(stubData);
        done();
      }
      localVue.prototype.$requestApi.get("test", callback);
    });

  test('get execute expected callback and catch result as failure ' +
    'if response is failure', done => {
      const localVue = createLocalVue();
      localVue.use(RequestApi);

      let stubData = { test_a: 1, test_b: "hello" };
      const resp = { status: 404, data: stubData };
      let resolved = Promise.reject(resp);
      axios.get.mockImplementation(
        () => {
          return resolved;
        }
      );
      function callback(data) {
        expect(data.result).toBe(false);
        expect(data.data.status).toBe(404);
        done();
      }
      localVue.prototype.$requestApi.get("test", callback);
    });
  test('post is callable', done => {
    axios.post.mockImplementation(
      () => {
        return Promise.resolve({});
      }
    );
    function callback(data) {
      expect(data.result).toBe(true);
      done();
    }
    localVue.prototype.$requestApi.post("test", callback, { a: "a", b: "b" });
  });
});